<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Project 2 - Server Side Validation">
	<meta name="author" content="Dathan Busby">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Project 2</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../p2/global/header.php"); ?>	
					</div>

					<h2>Pet Stores</h2>
						<?php //be sure to change action="#"?>
						<form id="edit_petstore" method="post" class="form-horizontal" action="edit_petstore_process.php">
                                <?php
								
								require_once "global/connection.php";
								
								$pst_id_v =$_POST['pst_id'];
								
								$query =
								"SELECT *
								FROM petstore
								WHERE pst_id = :pst_id_p";
								
								$statement = $db->prepare($query);
								$statement->bindParam(':pst_id_p',$pst_id_v);
								$statement->execute();
								$result = $statement->fetch();
								while($result != null)
								{
								?>
								    <input type="hidden" name="pst_id" value="<?php echo $result['pst_id']; ?>"/>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="name" value="<?php echo $result['pst_name']; ?>" placeholder="(max 30 characters)" />
										</div>
                					</div>
             
             					<div class="form-group">
								 		<label class="col-sm-4 control-label">Street:</label>
										 <div class="col-sm-4">
										 	<input type="text" class="form-control" name="street" value="<?php echo $result['pst_street']; ?>" placeholder="(max 30 characters)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="city" value="<?php echo $result['pst_city']; ?>" placeholder="(Letters and Numbers Only)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="state" value="<?php echo $result['pst_state']; ?>" placeholder="(Example: FL)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Zip:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="zip" value="<?php echo $result['pst_zip']; ?>" placeholder="(5 or 9 digits, no dashes)" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="phone" value="<?php echo $result['pst_phone']; ?>" placeholder="(10 digits no other characters)" />
										</div>
									</div>
             
								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="email" value="<?php echo $result['pst_email']; ?>" placeholder="Example: jdoe@gmail.com" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">URL:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="url" value="<?php echo $result['pst_url']; ?>" placeholder="Example: www.google.com" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">YTD Sales:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="ytdsales" value="<?php echo $result['pst_ytd_sales']; ?>" placeholder="(11 digits max including decimal)" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="notes" value="<?php echo $result['pst_notes']; ?>" />
										</div>
									</div>

                                <?php
								$result = $statement->fetch();
								}
								$db = null;
								?>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="update" value="Update">Update</button>
										</div>
									</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>


</body>
</html>
