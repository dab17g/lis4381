# Assignment 1

### Assignment 1 Requirements:
1. Screenshot of AMPPS
2. Screenshot of java Hello
3. Screenshot of Android Studi - my first app
4. Git command definitions
5. Bitbucket repo links for tutorials and this assignment

### Git command descriptions:
1. git innit: used to initialize a local directory to become a git repository. The first thing you should do when making a repository.
2. git status: Displays the status of the directory. Also shows the files in the staging area.
3. git add: adds the files that have been changed or added to the staging area.
4. git commmit: Takes a screenshot of the files/changes in the staging area.
5. git push: Pushes the commit to the remote repository.
6. git pull: Downloads the files/changes that are present in the remote repository but not the local repository to the local repository.
7. git clone: Used to clone a remote repository to a local directory.

### Screenshots
![ampps](https://bitbucket.org/dab17g/lis4381/raw/e48f2a44edaa6bb406976323c0fac3540df966e3/a1/images/correctampps.PNG)
![javaHello](https://bitbucket.org/dab17g/lis4381/raw/8bc552eda70a8695de6a44f57fdd0b6d164a9d86/a1/images/javaHello.PNG)
![androidstudio](https://bitbucket.org/dab17g/lis4381/raw/8bc552eda70a8695de6a44f57fdd0b6d164a9d86/a1/images/androidstudio.PNG)

### tutorial link
bitbucket tutorial link: https://bitbucket.org/dab17g/bitbucketstationlocations/src/master/

