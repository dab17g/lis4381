# LIS4381 - Mobile Web Application Development

## Dathan Busby

## Assignment 5

### Assignment Requirements:
1. Create form for user entered data that uses server-side validation
2. Use Regular Expressions with jQuery validation to verify inputs as valid
3. Link mySQL database to input data
4. Display data using a table created with html and php

#### Assignment Screenshots:

![alt](https://bitbucket.org/dab17g/lis4381/raw/2c1f4e638cbfbc4b6e49b5506b2eb63663af92bc/a5/img/a5_1.PNG)
![alt](https://bitbucket.org/dab17g/lis4381/raw/2c1f4e638cbfbc4b6e49b5506b2eb63663af92bc/a5/img/a5_2.PNG)

#### [Local Web App](http://localhost/lis4381/a5/index.php)
