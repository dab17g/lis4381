# Assignment 3

### Assignment 3 Requirements:
1. Screenshot of ERD
2. Screenshot of first app screen.
3. Screenshot of second app screen.
4. Link to a3.mwb and a3.sql

### Screenshots
![erd](https://bitbucket.org/dab17g/lis4381/raw/be8b7290816b8c28fc786ea8a5f19e6c2ddb3fc4/a3/requirements/erd.png)
![first](https://bitbucket.org/dab17g/lis4381/raw/be8b7290816b8c28fc786ea8a5f19e6c2ddb3fc4/a3/requirements/first.PNG)
![second](https://bitbucket.org/dab17g/lis4381/raw/be8b7290816b8c28fc786ea8a5f19e6c2ddb3fc4/a3/requirements/second.PNG)

### tutorial link
Link to a3.sql: https://bitbucket.org/dab17g/lis4381/src/master/a3/requirements/a3_busby.sql

Link to a3.mwb: https://bitbucket.org/dab17g/lis4381/src/master/a3/requirements/a3_erd_busby.mwb

