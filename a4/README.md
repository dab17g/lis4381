# Assignment 4

# requirements
1. clone git files
2. modify index.php
3. provide carousel screenshot
4. provide validation working and not working.

# screenshots
![validation1](https://bitbucket.org/dab17g/lis4381/raw/57b9642996ec72b58fe14679b8ba094d20524d47/a4/screenshots/no_validation.PNG)
![validation](https://bitbucket.org/dab17g/lis4381/raw/57b9642996ec72b58fe14679b8ba094d20524d47/a4/screenshots/validation.PNG)
![slideshow](https://bitbucket.org/dab17g/lis4381/raw/57b9642996ec72b58fe14679b8ba094d20524d47/a4/screenshots/slideshow.png)
