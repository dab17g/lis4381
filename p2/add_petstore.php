<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Assignment 5 - Server Side Validation">
	<meta name="author" content="Dathan Busby">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment5</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../a5/global/header.php"); ?>	
					</div>

					<h2>Pet Stores</h2>
						<?php //be sure to change action="#"?>
						<form id="add_store_form" method="post" class="form-horizontal" action="add_petstore_process.php">
								<div class="form-group">
										<label class="col-sm-4 control-label">Name:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="name" placeholder="(max 30 characters)" />
										</div>
                					</div>
             
             					<div class="form-group">
								 		<label class="col-sm-4 control-label">Street:</label>
										 <div class="col-sm-4">
										 	<input type="text" class="form-control" name="street" placeholder="(max 30 characters)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">City:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="city" placeholder="(Letters and Numbers Only)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">State:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="state" placeholder="(Example: FL)" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">Zip:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="zip" placeholder="(5 or 9 digits, no dashes)" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Phone:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="phone" placeholder="(10 digits no other characters)" />
										</div>
									</div>
             
								<div class="form-group">
										<label class="col-sm-4 control-label">Email:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="email" placeholder="Example: jdoe@gmail.com" />
										</div>
									</div>

								<div class="form-group">
										<label class="col-sm-4 control-label">URL:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="url" placeholder="Example: www.google.com" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">YTD Sales:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="ytdsales" placeholder="(11 digits max including decimal)" />
										</div>
									</div>
								
								<div class="form-group">
										<label class="col-sm-4 control-label">Notes:</label>
										<div class="col-sm-4">
											<input type="text" class="form-control" name="notes" />
										</div>
									</div>

								<div class="form-group">
									<div class="col-sm-6 col-sm-offset-3">
									<button type="submit" class="btn btn-primary" name="add" value="Add">Add</button>
										</div>
									</div>
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>



</body>
</html>
