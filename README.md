# LIS 4381 - Mobile Web Application Development

## Dathan Busby

### Assignment Links:

1. [A1 README.md](https://bitbucket.org/dab17g/lis4381/src/master/a1/README.md)
	- Install AMPS
	- Install JDK
	- Install Android Studio and create app
	- Provide screenshots
	- Create Bitbucket repo
	- Create Bitbucket tutorials
	- Provide git command descriptions
	
2. A2 README.md
	- Create healthy recipes android app
	- Provide screenshots of completed app
	
3. [A3 README.md](https://bitbucket.org/dab17g/lis4381/src/master/a3/README.md)
	- Create MyEvent App
	- Create ERD and database in MySQL
	- Provide screenshots and links

4. [A4 README.md](https://bitbucket.org/dab17g/lis4381/src/master/a4/README.md)
	- Create Main menu webpage with slideshow
	- Create form validation (client-side)
	- Provide screenshots for both pages
	
5. [A5 README.md](https://bitbucket.org/dab17g/lis4381/src/master/a5/README.md)
	- Create form
	- server side validation
	- link to web app

6. [P1 README.md](https://bitbucket.org/dab17g/lis4381/src/master/p1/README.md)
	- Create Business Card App.
	- Provide screenshot of first page.
	- Provide screenshot of second page.

7. [P2 README.md](https://bitbucket.org/dab17g/lis4381/src/master/p2/README.md)
	- Provide screenshot edit page
	- Provide screenshot of error message
	- Provide screenshot of rss feed