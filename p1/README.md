# Project 1

Dathan Busby, LIS4381

### Project 1 Requirements:
1. Screenshot of first app screen.
2. Screenshot of second app screen.

### Screenshots
![first](https://bitbucket.org/dab17g/lis4381/raw/eaae1dda1323889c2f2b38c95351dfee618300d9/p1/first_about.PNG)
![second](https://bitbucket.org/dab17g/lis4381/raw/eaae1dda1323889c2f2b38c95351dfee618300d9/p1/second_about.PNG)

